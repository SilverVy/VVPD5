"""Серебров Вячеслав ВВПД №5"""
from random import randint
import sys


class HotelRoom:
    """
    Класс представляет собой отдельный номер в отеле.
    Обладает различными методами для работы с номером отеля.

    :param room_number: Номер комнаты.
    :type room_number: int
    :param room_type: Тип комнаты (стандарт, люкс и т.д.).
    :type room_type: str
    :param rate: Стоимость проживания в комнате.
    :type rate: int
    :param is_available: Флаг, указывающий, доступна ли комната для бронирования.
    :type is_available: bool

    :raises ValueError: Если переданы некорректные значения параметров.

    :return: Объект HotelRoom.

    :Example:
    >>> room = HotelRoom(101, "стандарт", 120, True)
    """

    def __init__(self, room_number, room_type, rate, is_available):
        """
        Инициализирует объект HotelRoom.
        Конструктор класса, инициализирует атрибуты комнаты при её создании.

        :param room_number: Номер комнаты.
        :type room_number: int
        :param room_type: Тип комнаты (стандарт, люкс и т.д.).
        :type room_type: str
        :param rate: Стоимость проживания в комнате.
        :type rate: int
        :param is_available: Флаг, указывающий, доступна ли комната для бронирования.
        :type is_available: bool

        :raises ValueError: Если переданы некорректные значения параметров.

        :return: None
        """

        self.room_number = room_number
        self.room_type = room_type
        self.rate = rate
        self.is_available = is_available

    def book_room(self):
        """
        Забронировать комнату, если она доступна.

        :return: Сообщение о статусе бронирования комнаты.
        :rtype: str
        :raises: None

        :Example:
        >>> room = HotelRoom(101, "стандарт", 120, True)
        >>> room.book_room()
        'Номер 101 (стандарт) успешно забронирован по цене $120.'
        """

        if self.is_available:
            self.is_available = False
            return f"Номер {self.room_number} ({self.room_type}" \
                   f") успешно забронирован по цене ${self.rate}."
        return f"Извините, номер {self.room_number}" \
                   f" ({self.room_type}) уже занят."


    def check_availability(self):
        """
        Проверить доступность комнаты.

        :return: True, если комната доступна, иначе False.
        :rtype: bool
        :raises: None

        :Example:
        >>> room = HotelRoom(101, "стандарт", 120, True)
        >>> room.check_availability()
        True
        """

        return self.is_available


    def display_info(self):
        """
        Вывести информацию о комнате.

        :return: None
        :rtype: None
        :raises: None

        :Example:
        >>> room = HotelRoom(101, "стандарт", 120, True)
        >>> room.display_info()
        Номер: 101  Класс: стандарт  Цена: 120  Свободен: True
        """

        print(f"Номер: {self.room_number}  Класс: {self.room_type}" \
              f"  Цена: {self.rate}  Свободен: {self.is_available}")

    def cancel_reservation(self):
        """
        Снять бронь с комнаты, если она забронирована.

        :return: None
        :rtype: None
        :raises: None

        :Example:
        >>> room = HotelRoom(101, "стандарт", 120, False)
        >>> room.cancel_reservation()
        Ваша бронь с номера 101 снята
        """

        if self.is_available is False:
            self.is_available = True
            print(f'Ваша бронь с номера {self.room_number} снята')
        else:
            print(f'На номере {self.room_number} нет брони')



def menu():
    """
    Основная функция управления отелем.

    Она содержит следующие шаги:
    Запрашивает у пользователя количество номеров в отеле.
    Генерирует стандартные и люксовые номера с случайными параметрами.
    Организует бесконечный цикл для взаимодействия с меню пользователя, предоставляя следующие опции:
    Показать информацию о номерах отеля.
    Вывести свободные номера.
    Забронировать номер.
    Проверить конкретный номер на бронь.
    Снять бронь с номера.
    Выход из программы.


    :return: None
    :rtype: None
    :raises: None

    :Example:
    >>> menu()
    """

    cnt = 0
    my_rooms = []
    while True:
        try:
            if cnt > 0:
                break
            count_rooms = int(input('Введите количество номеров в отеле: '))
            hotel_rooms_standart = [HotelRoom(number, "стандарт" \
                                              , randint(100, 150), bool \
                                                  (randint(0, 1))) for number in \
                                    range(1, (count_rooms // 2) + 1)]
            hotel_rooms_luxury = [HotelRoom(number, "люкс" \
                                            , randint(250, 350), bool \
                                                (randint(0, 1))) for number in \
                                  range((count_rooms // 2) + 1, \
                                        count_rooms + 1)]
            hotel_rooms = hotel_rooms_standart + hotel_rooms_luxury
            cnt += 1
            break
        except ValueError:
            print('Введено неверное значение, попробуйте снова \n')
            continue
    while True:
        try:
            print("")
            print('1. Показать информацию о номерах отеля \n'
                  '2. Вывести свободные номера \n'
                  '3. Забронировать номер \n'
                  '4. Проверить конкретный номер на бронь \n'
                  '5. Снять бронь с номера \n'
                  '6. Выход \n'
                  )
            action = input('Введите номера команд: ')
            if action == '1':
                for schet in range(count_rooms):
                    hotel_rooms[schet].display_info()
            elif action == '2':
                free_rooms = []
                for number in range(count_rooms):
                    if hotel_rooms[number].check_availability():
                        free_rooms.append(number + 1)
                print(f'Свободные номера: {free_rooms}')
            elif action == '3':
                room_number = int(input("Введите номер комнаты: "))
                flag = hotel_rooms[room_number - 1].check_availability()
                print(hotel_rooms[room_number - 1].book_room())
                if (flag is True) and hotel_rooms[room_number - 1] \
                        .check_availability() is False:
                    my_rooms.append(room_number)
            elif action == '4':
                room_number = int(input("Введите номер комнаты: "))
                if hotel_rooms[room_number - 1].check_availability():
                    print('Данный номер свободен')
                else:
                    print('Этот номер уже забронирован')
            elif action == '5':
                room_number = int(input("Введите номер комнаты: "))
                if room_number in my_rooms:
                    hotel_rooms[room_number - 1].cancel_reservation()
                else:
                    if hotel_rooms[room_number - 1].check_availability():
                        print('Этот номер никем не забронирован')
                    else:
                        print('Этот номер забронирован не вами')
            elif action == '6':
                sys.exit()
            else:
                print('Введено неверное значение, попробуйте снова \n')
        except ValueError:
            print('Введено неверное значение, попробуйте снова \n')


if __name__ == '__main__':
    menu()

