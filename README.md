# Система управления отелем


## Содержание:

1.	*О программе*
2.	*Использование*
    1. *Создание объекта HotelRoom*
    2. *Бронирование комнаты*
    3. *Проверка доступности комнаты*
    4. *Отображение информации о комнате*
    5. *Снятие брони с комнаты*
3.	*Пример использования*
4.	*Меню управления отелем*
5.	*Опции меню*
6.	*Формула количества стандартных и люкс номеров*
7. *Список задач для завершения работы над проектом*
8. *Различные источники*
9. *Фото исполнения программы*

## О программе

Программа представляет собой систему управления номерами в отеле. Реализован класс **HotelRoom**, представляющий отдельный номер, с методами для бронирования, проверки доступности, вывода информации и отмены брони. Также есть основная функция **menu()**, предоставляющая пользователю опции для взаимодействия с системой управления.

## Использование

- **Создание объекта HotelRoom**:
Используйте конструктор класса для создания объекта HotelRoom. Пример:

```python
room = HotelRoom(101, "стандарт", 120, True)
```

- **Бронирование комнаты**:
Используйте метод book_room() для бронирования комнаты. Пример:

```python
room.book_room()
```

- **Проверка доступности комнаты**:
Используйте метод check_availability() для проверки доступности комнаты. Пример:

```python
room.check_availability()
```

- **Отображение информации о комнате**:
Используйте метод display_info() для вывода информации о комнате. Пример:

```python
room.display_info() 
```

- **Снятие брони с комнаты**:
Используйте метод cancel_reservation() для снятия брони с комнаты. Пример:

```python
room.cancel_reservation() 
```

## Пример использования

```python
room = HotelRoom(101, "стандарт", 120, True)
room.display_info()
room.book_room() 
room.display_info() 
room.cancel_reservation() 
```

## Меню управления отелем

Запустите функцию **menu()** для управления отелем через текстовое меню. Пример:
```python
menu() 
```

## Опции меню

+ Показать информацию о номерах отеля.
+ Вывести свободные номера.
+ Забронировать номер.
+ Проверить конкретный номер на бронь.
+ Снять бронь с номера.
+ Выход из программы.

## Формула количества стандартных и люкс номеров

При введении $N$ *числа номеров* в отеле, номера распределяются так:

$$ x_{люкс}, x_{стандарт} = {N \over 2} $$

Если $N$ - нечетное, то номеров *люкс* будет **на один больше**.

## Список задач для завершения работы над проектом

- [X] Написание кода
- [X] Документация
- [ ] Сдача проекта

## Различные источники
- [Ссылка на практическую работу](https://e.sfu-kras.ru/mod/assign/view.php?id=1516444)
- [Гайд по оформлению README](https://www.opensourceagenda.com/projects/format-readme)

## Фото исполнения программы

![Пример выполнения программы №1](https://sun9-14.userapi.com/impg/Hbr9A2pwpzRY7m7M8OjCh9Ene1L2Vn0nP7YXzA/_Um7qhnAWqU.jpg?size=610x235&quality=96&sign=2cbc05a4766a62374e5044e2ee4f8bf6&type=album "1")

![Пример выполнения программы №2](https://sun125-1.userapi.com/impg/AH_g7TXXG8eaqiTmpWNmL2BOWmX-QQD6VYEXTA/ckZWYAOQaEw.jpg?size=484x140&quality=96&sign=267dad306bfd947e07a3751c965cb00f&type=album "2")

![Пример выполнения программы №3](https://sun125-1.userapi.com/impg/htwL_8l0mP3U_zgtVqM41JcR9A1FLJ9SEegVCg/1hP15T7ARW4.jpg?size=439x75&quality=96&sign=49e3deabade4aaf75ca9d989531b7674&type=album "3")




 



